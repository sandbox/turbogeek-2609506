DESCRIPTION
===========

Displays a download link in the IMCE UI for easy downloading of a file or multiple files.


REQUIREMENTS
=============

IMCE module.


INSTALLATION
=============

Download and enable the module.
Read more about installing modules at http://drupal.org/node/70151


CONFIGURATION AND USAGE
=============

Enabling the module will display the download link in the IMCE UI.
Select one or more files in the browser and click on the Download icon.


AUTHOR
============

Shane Graham "turbogeek" (http://drupal.org/user/266729)
http://www.deckfifty.com