(function ($) {

  // Add hook.load
  imce.hooks.load.push(function () {
    // Add download operation.
    imce.opAdd({
      name: 'download',
      title: Drupal.t('Download')
    });
    // Enable download operation.
    imce.opEnable('download');
    // Add download callback.
    imce.ops['download'].func = imce.downloadAction;
  });

  // Download file action.
  imce.downloadAction = function(show) {
    if (show) {
      for (var fid in imce.selected) {
        imce.send(fid);
      }
      if (imce.selcount != 1) {
        imce.setMessage(Drupal.t('Please select a file or files to download.'), 'error');
      }
    }
  };

})(jQuery);




